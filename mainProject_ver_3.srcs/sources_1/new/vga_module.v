`timescale 1ns / 1ps
module vga_module(
   
    //SysBus KVARK
	input			BusClk,
	input			Rst,
	input	[31:0]	Addr,
	input	[2:0]	Burst,
	input	[1:0]	Trans,
	input			BurstLen,
	input			MemAccess,
	input			Write,
	input	[31:0]	Wdata,

	output			Ready,
	output	[31:0]	RData,

	//VGA signals
	output	[3:0]	VGA_R,
	output	[3:0]	VGA_G,
	output	[3:0]	VGA_B,
	output          VGA_HS,
	output          VGA_VS,
	
	input           clk_25,
    input   [11:0]  SW_12,
	output hburst_err_ext
    );
    
     // VGA timings https://timetoexplore.net/blog/video-timings-vga-720p-1080p
     // 640x480 resolution
     localparam HS_STA = 16;              // horizontal sync start
     localparam HS_END = 16 + 96;         // horizontal sync end
     localparam HA_STA = 16 + 96 + 48;    // horizontal active pixel start
     localparam VS_STA = 480 + 10;        // vertical sync start
     localparam VS_END = 480 + 10 + 2;    // vertical sync end
     localparam VA_END = 480;             // vertical active pixel end
     localparam LINE   = 800;             // complete line (pixels)
     localparam SCREEN = 525;             // complete screen (lines)
    
    
    wire clk = BusClk;
    
    assign Ready = 1'b1;
    assign hburst_err_ext = 1'b0;
    
    reg	   [31:0]  Addr_r;
    reg    [2:0]   Burst_r;
    reg    [1:0]   Trans_r;
    reg            BurstLen_r;
    reg            MemAccess_r;
    reg            Write_r;
    reg [31:0] RData_r;
   
   always @(posedge BusClk or posedge Rst) begin
        if (Rst) begin
            Addr_r <= 'b0;
            Burst_r <= 'b0;
            Trans_r <= 'b0;
            BurstLen_r <= 'b0;
            MemAccess_r <= 'b0;
            Write_r <= 'b0;
        end
        else begin
            Addr_r <= Addr;
            Burst_r <= Burst;
            Trans_r <= Trans;
            BurstLen_r <= BurstLen;
            MemAccess_r <= MemAccess;
            Write_r <= Write;
        end
    end
    assign RData = RData_r;
    reg [9:0] x = 10'b0;
    reg [9:0] y;
    
    reg [9:0] posx = 320;
    reg [9:0] posy = 240;
    reg [9:0] size = 10;
    localparam WALLS_COUNT = 16;
    reg [9:0] wallsx [WALLS_COUNT - 1:0];
    reg [9:0] wallsy [WALLS_COUNT - 1:0];    

    always @(posedge BusClk or posedge Rst) begin
        if (Rst) begin
            RData_r <= 32'b0;
        end
        else if (Trans == 2'b11 && MemAccess && ~Write) begin
            case (Addr[3:0])
                4'h0: RData_r <= {22'b0, posx};
                4'h4: RData_r <= {22'b0, posy};
                4'h8: RData_r <= {22'b0, size};
                default: RData_r <= 32'b0;
            endcase
        end
    end
    integer i;
    always @(posedge BusClk or posedge Rst) begin
        if (Rst) begin
            posx <= 'd320;
            posy <= 'd240;
            size <= 'd10;
            for(i=0; i<WALLS_COUNT; i=i+1)
            begin
                    wallsx[i] <= 10'd0;
                    wallsy[i] <= 10'd0; 
            end
        end
        else if (Trans_r == 2'b11 && MemAccess_r && Write_r) begin
            case (Addr_r[3:0])
                4'h0: begin
                    posx <= Wdata[9:0];
                end
                4'h4: begin
                    posy <= Wdata[9:0];
                end
                4'h8: size <= Wdata[9:0];
                4'hC: begin
                   wallsx[Wdata[23:20]] <= Wdata[9:0];
                   wallsy[Wdata[23:20]] <= Wdata[19:10];
                end
            endcase
        end 
    end
    
        
    
    /////////////////////////////////////////////////
    reg [3:0] vga_R_r;
    reg [3:0] vga_G_r;
    reg [3:0] vga_B_r;
    
    reg [18:0] cntr;
    
    always @(posedge clk or posedge Rst)
    if (Rst) begin
        vga_R_r <= 4'h0;
        vga_G_r <= 4'hC;
        vga_B_r <= 4'h8;
        cntr <= 1'b0;
    end
    else begin
        if (cntr == 19'b0) begin
            vga_R_r <= vga_R_r + 1'b1; //vga_R_r + 1'b1;
            vga_G_r <= vga_G_r + 1'b1; //vga_G_r + 1'b1;
            vga_B_r <= vga_B_r + 1'b1; //vga_B_r + 1'b1;
        end
        else cntr <= cntr + 1'b1;
    end
    
    wire in_square = (x > (posx - size / 2)) && (x < (posx + size / 2)) && (y > (posy - size / 2)) && (y < (posy + size / 2));
    reg [WALLS_COUNT - 1 : 0] in_wall_r = {WALLS_COUNT{1'b0}};
    wire in_wall = |in_wall_r;
    
    always@(posedge clk or posedge Rst)
    if (Rst)
        in_wall_r <= {WALLS_COUNT{1'b0}};
    else begin
        for(i = 0; i < WALLS_COUNT; i = i + 1)
            begin 
                if ((x > (wallsx[i] - size / 2)) && (x < (wallsx[i] + size / 2)) && (y > (wallsy[i] - size / 2)) && (y < (wallsy[i] + size / 2)))
                    in_wall_r[i] <= 1'b1;
                else in_wall_r[i] <= 1'b0;
            end
            /*if ((x > (wallsx[0] - size / 2)) && (x < (wallsx[0] + size / 2)) && (y > (wallsy[0] - size / 2)) && (y < (wallsy[0] + size / 2)))
                in_wall_r <= 16'b1;//in_wall_r | (1'b1 << i);
            else  if ((x > (wallsx[1] - size / 2)) && (x < (wallsx[1] + size / 2)) && (y > (wallsy[1] - size / 2)) && (y < (wallsy[1] + size / 2)))
                in_wall_r <= 16'b1;//in_wall_r | (1'b1 << i);
            else  if ((x > (wallsx[2] - size / 2)) && (x < (wallsx[2] + size / 2)) && (y > (wallsy[2] - size / 2)) && (y < (wallsy[2] + size / 2)))
                in_wall_r <= 16'b1;//in_wall_r | (1'b1 << i);
            else  if ((x > (wallsx[3] - size / 2)) && (x < (wallsx[3] + size / 2)) && (y > (wallsy[3] - size / 2)) && (y < (wallsy[3] + size / 2)))
                in_wall_r <= 16'b1;//in_wall_r | (1'b1 << i);
            else  if ((x > (wallsx[4] - size / 2)) && (x < (wallsx[4] + size / 2)) && (y > (wallsy[4] - size / 2)) && (y < (wallsy[4] + size / 2)))
                in_wall_r <= 16'b1;// in_wall_r | (1'b1 << i);
            else  if ((x > (wallsx[5] - size / 2)) && (x < (wallsx[5] + size / 2)) && (y > (wallsy[5] - size / 2)) && (y < (wallsy[5] + size / 2)))
                in_wall_r <= 16'b1;//in_wall_r | (1'b1 << i);
            else  if ((x > (wallsx[6] - size / 2)) && (x < (wallsx[6] + size / 2)) && (y > (wallsy[6] - size / 2)) && (y < (wallsy[6] + size / 2)))
                in_wall_r <= 16'b1;//in_wall_r | (1'b1 << i);
            else  if ((x > (wallsx[7] - size / 2)) && (x < (wallsx[7] + size / 2)) && (y > (wallsy[7] - size / 2)) && (y < (wallsy[7] + size / 2)))
                in_wall_r <= 16'b1;//in_wall_r | (1'b1 << i);
            else in_wall_r <= 16'b0;*/
    end
    
    assign VGA_HS = x < (640 + 16) || x >= (640 + 16 + 96);
    assign VGA_VS = y < (480 + 10) || y >= (480 + 10 + 2);
    wire valid = (x < 640) && (y < 480);
    
    
    assign VGA_R = ((valid) ? ((in_square) ? SW_12[11:8] : ((in_wall) ? ~SW_12[11:8] : 4'b1111)) : 4'b0);
    assign VGA_G = ((valid) ? ((in_square) ? SW_12[7:4] : ((in_wall) ? ~SW_12[7:4] : 4'b1111)) : 4'b0);
    assign VGA_B = ((valid) ? ((in_square) ? SW_12[3:0] : ((in_wall) ? ~SW_12[3:0] : 4'b1111)) : 4'b0);
   
    
    always @(posedge clk_25) begin
            if (Rst) begin
                x <= 10'b0;
                y <= 10'b0;
            end else begin
                if (x < 10'd799) begin
                    x <= x + 1'b1;
                end else begin
                    x <= 10'b0;
                    if (y < 10'd524) begin
                        y <= y + 1'b1;
                    end else begin
                        y <= 10'b0;
                    end
                end
            end
        end

    
    
    
   
endmodule

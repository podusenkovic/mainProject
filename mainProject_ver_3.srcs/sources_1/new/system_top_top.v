`timescale 1ns / 1ps

module system_top_top(
	input			CLK,
	input			RST,

	input			UART_TXD_IN,
	output			UART_RXD_OUT,
	
	inout	[4:1]	JA,
	inout	[4:1]	JB,
	inout	[15:0]	LED,
	inout	[15:0]	SW,
	
	output   [3:0]   VGA_R,
    output   [3:0]   VGA_G,
    output   [3:0]   VGA_B,
    output           VGA_HS,
    output           VGA_VS
	
);

reg flag;

always @(posedge CLK) begin
	if(JB[1] == 0) flag <= 1;
end

assign LED[14] = flag;

system_top system_top_inst(
	.CLK(CLK),
	.RST(RST),

	.GPIO({LED[11:0], SW[15:12]}),
    .SW_12(SW[11:0]),
	// JTAG
	.TCK(JA[1]),
	.TDI(JA[2]),
	.TDO(JA[3]),
	.TMS(JA[4]),

	.led(LED[15]),

	.RX(UART_TXD_IN),
	.TX(UART_RXD_OUT),

	.SPI_CS_n(JB[1]),
	.SPI_MOSI(JB[2]),
	.SPI_MISO(JB[3]),
	.SPI_CLK(JB[4]),
	
	.VGA_R(VGA_R),
    .VGA_G(VGA_G),
    .VGA_B(VGA_B),
    .VGA_HS(VGA_HS),
    .VGA_VS(VGA_VS)
	);
endmodule

`timescale 1ns / 1ps
module spi_overlay(
	clk,
	rst,
	SPI_MISO,
	addr,
	memAccess,
	wdata,
	write,

	SPI_CLK,
	SPI_MOSI,
	SPI_CS_n,
	rdata,
	reply,
	error
);

input clk;
input rst;
input SPI_MISO;
input [31:0] addr;
input memAccess;
input [31:0] wdata;
input write;

output SPI_CLK;
output SPI_MOSI;
output SPI_CS_n;
output reg [31:0] rdata;
output reply;
output error;

wire [1:0] RX_Count;
wire RX_DV;
wire [7:0] RX_Byte;
wire TX_Ready;

reg [7:0] dataToRead = 8'b0;
reg [15:0] dataToWrite = 16'b0;
reg [7:0] TX_Byte = 8'b0;
reg [7:0] BYTE_TO_SEND_TO_SLAVE = 8'b0;
reg TX_DV = 1'b0;

reg [31:0] addr_r = 32'b0;
reg memAccess_r = 1'b0;
reg write_r = 1'b0;

reg flag = 1'b0;

wire ReadFlag;
wire WriteFlag;


assign reply = 1'b1;
assign error = 1'b0;


assign ReadFlag = (RX_Count == 2'b10 && RX_DV) ? 1'b1 : 1'b0;
assign WriteFlag = (SPI_CS_n && TX_Ready) ? 1'b1 : 1'b0;

always @(posedge clk or posedge rst) begin
	if(rst) begin
		dataToRead <= 8'b0;
		//ReadFlag <= 1'b0;
	end 
	else if (/*RX_DV == 1'b1 && !SPI_CS_n && RX_Count == 2'b10*/ReadFlag) begin
		dataToRead <= RX_Byte;
		//ReadFlag <= 1'b1;
	end 
	//else if(SPI_CS_n) ReadFlag <= 1'b0;
end

always @(posedge clk or posedge rst) begin
	if (rst) begin
		addr_r <= 'b0;
		memAccess_r <= 'b0;
		write_r <= 'b0;
	end
	else begin
		addr_r <= addr;
		memAccess_r <= memAccess;
		write_r <= write;
	end
end

always @(posedge clk or posedge rst) begin 
	if(rst) begin
		 rdata <= 32'b0;
	end 
	else if (memAccess && ~write) begin
		case (addr[3:0])
			4'h0: begin
				rdata <= {{24{dataToRead[7]}},dataToRead};
			end
			4'h4: rdata <= dataToWrite;
			4'h8: rdata <= {24'b0, BYTE_TO_SEND_TO_SLAVE};
			4'hC: begin
				rdata <= {30'b0, WriteFlag, ReadFlag};
			end
			default: rdata <= 32'b0;
		endcase
	end
end

always @(posedge clk or posedge rst) begin
	if(rst) begin
		dataToWrite <= 15'b0;
		BYTE_TO_SEND_TO_SLAVE <= 8'b0;
		flag <= 1'b0;
	end 
	else if (memAccess_r && write_r) begin
		case (addr_r[3:0])
			4'h4: begin
				dataToWrite <= wdata[15:0];
				flag <= 1'b1;
			end
			4'h8: BYTE_TO_SEND_TO_SLAVE <= wdata[7:0];
			default: ;
		endcase;
	end
	else flag <= 1'b0;
end

always @(posedge clk or posedge rst) begin
	if(rst) begin
		TX_Byte <= 8'b0;
		TX_DV <= 1'b0;
	end
	else begin
		if(TX_Ready && flag && RX_Count == 2'b00 && SPI_CS_n) begin
			TX_Byte <= dataToWrite[15:8];
			TX_DV <= 1'b1;
		end
		else if (TX_Ready && RX_Count == 2'b01 && !SPI_CS_n) begin
			TX_Byte <= dataToWrite[7:0];
			TX_DV <= 1'b1;
		end 
		else if (TX_Ready && RX_Count == 2'b10 && !SPI_CS_n) begin
			TX_Byte <= BYTE_TO_SEND_TO_SLAVE; 
			TX_DV <= 1'b1;
		end 
		else TX_DV <= 1'b0;
	end
end

SPI_Master_With_Single_CS 
#(
	.SPI_MODE(0),
	.CLKS_PER_HALF_BIT(50),
	.MAX_BYTES_PER_CS(3), // 2
	.CS_INACTIVE_CLKS(3)
)	spi_inst(
	// Control/Data Signals,
	.i_Rst_L(!rst),				// FPGA Reset
	.i_Clk(clk),				// FPGA Clock

	// TX (MOSI) Signals
	.i_TX_Count(2'b11),			// Number of bytes per CS
	.i_TX_Byte(TX_Byte),		// Byte to transmit on MOSI
	.i_TX_DV(TX_DV),			// Data Valid Pulse with i_TX_Byte
	.o_TX_Ready(TX_Ready),		// Transmit Ready for Byte
	
	// RX (MISO) Signals
	.o_RX_Count(RX_Count),		// Index of RX'd byte
	.o_RX_DV(RX_DV),			// Data Valid pulse (1 clock cycle)
	.o_RX_Byte(RX_Byte),		// Byte received on MISO

	// SPI Interface
	.o_SPI_Clk(SPI_CLK),
	.i_SPI_MISO(SPI_MISO),
	.o_SPI_MOSI(SPI_MOSI),
	.o_SPI_CS_n(SPI_CS_n)
	);

endmodule

module system(
  input   clk_cpu,
  input   clk_32768,
  input   pwr_rst_in_n,
 
  output [15:0] gpio_do,
  output [15:0] gpio_en,
  input  [15:0] gpio_di,

  input    tck,
  input    tms,
  input    tdi,
  output   tdo,
 
  input [1:0] start_mode,
 
  output logic mem_access,            // MemAccess
  output logic hwrite,                // Write
  output logic burst_length,          // BurstLen
  output logic [1:0] bus_transaction, // Trans
  output logic [2:0] hburst,          // Burst
  output logic [31:0] haddr,          // Addr
  output logic [31:0] hwdata,         // WData
  input  [31:0] hrdata_ext,           // RData
  input  reply_ext,                   // Ready
  input  hburst_err_ext,              // Err
  input  irq_ext
  );


assign tdo = 1'b0;
assign gpio_do = 16'h0000;
assign gpio_en = 16'hffff;


wire reply_in = reply_ext;
wire clk_bus = clk_cpu;

reg [31:0] rdata;

initial
  begin
  mem_access      = 0 ;
  hwrite          = 'z;
  burst_length    = 0 ;
  bus_transaction = 'z;
  hburst          = 'z;
  haddr           = 'z;
  hwdata          = 'z;
  end


/* ***************** uart adress map *************** 
localparam UART_BASE_ADDR       = 32'hFFFC_2000;
localparam UART_DATA_RX_ADDR    = UART_BASE_ADDR + 4'h0;
localparam UART_DATA_TX_ADDR    = UART_BASE_ADDR + 4'h4;
localparam UART_PRESCALER_ADDR  = UART_BASE_ADDR + 4'h8;
localparam UART_STATUS_REG_ADDR = UART_BASE_ADDR + 4'hC;

/* ***************** uart baudrate *************** 
localparam UART_BAUDRATE   = 115200;
localparam UART_CLK        = 50_000_000;
localparam PRESCALER_VALUE = UART_CLK / (UART_BAUDRATE*8);

/* ***************** uart baudrate *************** 
localparam UART_SR_RX_READY_MASK = 32'h0000_0001;
localparam UART_SR_TX_READY_MASK = 32'h0000_0002;
localparam UART_SR_RX_ERROR_MASK = 32'h0000_000C;

/* ***************** SPI adress map *************** */
localparam SPI_BASE_ADDR       = 32'hFFFC_2010;
localparam SPI_DATA_RX_ADDR    = SPI_BASE_ADDR + 4'h0;
localparam SPI_DATA_TX_ADDR    = SPI_BASE_ADDR + 4'h4;
localparam SPI_STATUS_REG_ADDR = SPI_BASE_ADDR + 4'hC;

/* ***************** SPI mask *************** */
localparam SPI_SR_RX_READY_MASK = 32'h0000_0001;
localparam SPI_SR_TX_READY_MASK = 32'h0000_0002;

/* ****************** command script *************** */

initial
  begin
  #30
  wait(pwr_rst_in_n);
  #300;

  rdata = 0;
  while (!(rdata & SPI_SR_TX_READY_MASK))
   read(SPI_STATUS_REG_ADDR);

  write(SPI_DATA_TX_ADDR,32'h0000_0B09);
  
  rdata = 0;
  while (!(rdata & SPI_SR_RX_READY_MASK))
   read(SPI_STATUS_REG_ADDR);
   
   read(SPI_DATA_RX_ADDR);

// Попробуем считать ещё одно значение

  rdata = 0;
  while (!(rdata & SPI_SR_TX_READY_MASK))
   read(SPI_STATUS_REG_ADDR);

  write(SPI_DATA_TX_ADDR,32'h0000_0B0a);
  
  rdata = 0;
  while (!(rdata & SPI_SR_RX_READY_MASK))
   read(SPI_STATUS_REG_ADDR);
   read(SPI_DATA_RX_ADDR);

  #10000;
  $finish;
  end
/* ************************************************* */
/*
initial
  begin
  #30
  wait(pwr_rst_in_n);
  #300;
  // configurate baudrate uart
  write(UART_PRESCALER_ADDR,PRESCALER_VALUE);
  // send value
  write(UART_DATA_TX_ADDR,32'h0000_0055);
  // wait end transmit 
  rdata = 0;
  while (!(rdata & UART_SR_TX_READY_MASK))
   read(UART_STATUS_REG_ADDR);
  
  write(UART_DATA_TX_ADDR,32'h0000_0066);  
  
  rdata = 0;
  while (!(rdata & UART_SR_TX_READY_MASK))
   read(UART_STATUS_REG_ADDR);

  while (!(rdata & UART_SR_RX_READY_MASK))
   read(UART_STATUS_REG_ADDR);
   read(UART_DATA_RX_ADDR);
   read(UART_STATUS_REG_ADDR);

  $stop;
  end
/* ************************************************* */

task write;
  input [31:0] addr;
  input [31:0] wdata;

  begin
    wait(!clk_bus);
    wait(clk_bus); //Fac­tum­que est ves­pere et mane, di­es unus.

    mem_access = 1;
    hwrite = 1;
    bus_transaction = 'b11;
    hburst = 0;
    haddr = addr;

    wait(!clk_bus);
    wait(clk_bus); //Et fac­tum est ves­pere et mane, di­es secundus.

    mem_access = 0;
    hwrite = 'z;
    bus_transaction = 'z;
    hburst = 'z;
    haddr = 'z;

    hwdata = wdata;

    wait(reply_in);
    wait(!clk_bus);
    wait(clk_bus); //Et fac­tum est ves­pere et mane, di­es tertius.

    hwdata = 'z;

    $display("WRITE %h: %h",addr,wdata);
  end
endtask


task read;
  input  [31:0] addr;
  begin
    //logic [31:0] rdata;

    wait(!clk_bus);
    wait(clk_bus);

    mem_access = 1;
    hwrite = 0;
    bus_transaction = 'b11;
    hburst = 0;
    haddr = addr;

    wait(!clk_bus);
    wait(clk_bus);

    mem_access = 0;
    hwrite = 'z;
    bus_transaction = 'z;
    hburst = 'z;
    haddr = 'z;

    wait(reply_in);
    wait(!clk_bus);
    wait(clk_bus);

    rdata = hrdata_ext;

    //$display("READ %h: %h",addr,rdata);
    if(rdata != 32'b0) $display("Time:%d ns - READ %h: %h",$time,addr,rdata);
  end

endtask

endmodule
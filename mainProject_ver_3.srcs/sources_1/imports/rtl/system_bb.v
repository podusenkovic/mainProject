
module system(
 input   clk_cpu,
 input   clk_32768,
 input   pwr_rst_in_n,

`ifdef UART
 output  usart0_txmrxsd,
 input   usart0_rxmtxsin,
`endif

 output [15:0] gpio_do,
 output [15:0] gpio_en,
 input [15:0] gpio_di,

 input		tck,
 input		tms,
 input		tdi,
 output		tdo,

`ifdef SPI
 output  spi_misod,
 output  spi_mosid, 
 output  spi_sckd,
 output  spi_misoe,
 output  spi_mosie,
 output  spi_scke,
 input   spi_misoin,
 input   spi_mosiin,
 input   spi_sckin,
 input   spi_nssin,
 //output [2:0] spi_nsso_oe_n,
 output [2:0] spi_nsso_d,
`endif

`ifdef I2C
 input    i2c_sdain,
 input    i2c_sclin,
 output   i2c_sdad,
 output   i2c_scld,
 output   i2c_sdapu,
 output   i2c_sclpu,
`endif

 input [1:0] start_mode,

 output mem_access,
 output hwrite,
 output burst_length,
 output [1:0] bus_transaction,
 output [2:0] hburst, 
 output [31:0] haddr,
 output [31:0] hwdata,
 input  [31:0] hrdata_ext,
 input  reply_ext,
 input  hburst_err_ext,
 
 input irq_ext
 );



endmodule


module interconnect (

  input clk,
  input reset_n,
 
  input mem_access,
  input hwrite,
  input burst_length,
  input [1:0] bus_transaction,
  input [2:0] hburst, 
  input [31:0] haddr,
  input [31:0] hwdata,
  output reg [31:0] hrdata,
  output reg reply_ext,
  output reg hburst_err_ext, 
  
  
  output tx_0,
  input  rx_0,

  output tx_1,
  input  rx_1,

  input SPI_MISO,
  output SPI_CLK,
  output SPI_MOSI,
  output SPI_CS_n,
    
    output   [3:0]   VGA_R,
    output   [3:0]   VGA_G,
    output   [3:0]   VGA_B,
    output           VGA_HS,
    output           VGA_VS,
    
    input            clk_25,
    input   [11:0]   SW_12
);



  // Slave addressing
  localparam ADDR_DECODER_MASK    = 32'hFFFF_FFF0;
  localparam SLAVE_0_ADDR         = 32'hFFFC_2000;
  localparam SLAVE_SPI_ADDR       = 32'hFFFC_2010;
  localparam SLAVE_VGA_ADDR       = 32'hFFFC_2020;


  // Address decoder
  reg [4:0] multiplexer_select;
  
  localparam NO_SLAVE_SELECTED = 5'd0;
  localparam SLAVE_0_SELECTED  = 5'd1;
  localparam SLAVE_1_SELECTED  = 5'd2;
  localparam SLAVE_2_SELECTED  = 5'd3;

  always @(posedge clk or negedge reset_n) begin
    if(~reset_n)
      multiplexer_select <= NO_SLAVE_SELECTED;
    else begin
      case (haddr & ADDR_DECODER_MASK)
        SLAVE_0_ADDR    : multiplexer_select <= SLAVE_0_SELECTED;
        SLAVE_SPI_ADDR  : multiplexer_select <= SLAVE_1_SELECTED;
        SLAVE_VGA_ADDR  : multiplexer_select <= SLAVE_2_SELECTED;
        default         : multiplexer_select <= NO_SLAVE_SELECTED;
      endcase
    end
  end


  wire mem_access_0;
  wire mem_access_1;
  wire mem_access_2;

  assign mem_access_0 = mem_access && 
                       ((haddr & ADDR_DECODER_MASK) == SLAVE_0_ADDR);
  assign mem_access_1 = mem_access && 
                       ((haddr & ADDR_DECODER_MASK) == SLAVE_SPI_ADDR);
  assign mem_access_2 = mem_access && 
                       ((haddr & ADDR_DECODER_MASK) == SLAVE_VGA_ADDR);  
                     

  // Data multiplexer
  wire [31:0] hrdata_0;
  wire [31:0] hrdata_1;
  wire [31:0] hrdata_2;

  wire reply_ext_0;
  wire reply_ext_1;
  wire reply_ext_2;

  wire hburst_err_ext_0;
  wire hburst_err_ext_1;
  wire hburst_err_ext_2;


  always @(*) begin
    case (multiplexer_select)
      SLAVE_0_SELECTED : hrdata = hrdata_0;
      SLAVE_1_SELECTED : hrdata = hrdata_1;
      SLAVE_2_SELECTED : hrdata = hrdata_2;
      default          : hrdata = 32'dz;
    endcase
  end

  always @(*) begin
    case (multiplexer_select)
      SLAVE_0_SELECTED : hburst_err_ext = hburst_err_ext_0;
      SLAVE_1_SELECTED : hburst_err_ext = hburst_err_ext_1;
      SLAVE_2_SELECTED : hburst_err_ext = hburst_err_ext_2;
      default          : hburst_err_ext = 1'b0;
    endcase
  end

  always @(*) begin
    case (multiplexer_select)
      SLAVE_0_SELECTED : reply_ext = reply_ext_0;
      SLAVE_1_SELECTED : reply_ext = reply_ext_1;
      SLAVE_2_SELECTED : reply_ext = reply_ext_2;
      default          : reply_ext = 1'b1;
    endcase
  end


  // Uart Slave 0
  uart_bus_overlay uart_instance_0 (
    .BusClk         (clk),
    .Rst            (~reset_n),
    
    .MemAccess      (mem_access_0),
    .Write          (hwrite),
    .BurstLen       (burst_length),
    .Trans          (bus_transaction),
    .Burst          (hburst), 
    .Addr           (haddr),
    .Wdata          (hwdata),
    .RData          (hrdata_0),
    .Ready          (reply_ext_0),
    .hburst_err_ext (hburst_err_ext_0),

    .tx             (tx_0),
    .rx             (rx_0)
  );

spi_overlay spi_overlay_inst(
  .clk(clk),
  .rst(~reset_n),
  .SPI_MISO(SPI_MISO),
  .addr(haddr),
  .memAccess(mem_access_1),
  .wdata(hwdata),
  .write(hwrite),
  
  .SPI_CLK(SPI_CLK),
  .SPI_MOSI(SPI_MOSI),
  .SPI_CS_n(SPI_CS_n),
  .rdata(hrdata_1),
  .reply(reply_ext_1),
  .error(hburst_err_ext_1)
);

  // VGA Slave 2
  vga_module vga_module_inst (
    .BusClk         (clk),
    .Rst            (~reset_n),
    
    .MemAccess      (mem_access_2),
    .Write          (hwrite),
    .BurstLen       (burst_length),
    .Trans          (bus_transaction),
    .Burst          (hburst), 
    .Addr           (haddr),
    .Wdata          (hwdata),
    .RData          (hrdata_2),
    .Ready          (reply_ext_2),
    .hburst_err_ext (hburst_err_ext_2),

    
    .VGA_R(VGA_R),
    .VGA_G(VGA_G),
    .VGA_B(VGA_B),
    .VGA_HS(VGA_HS),
    .VGA_VS(VGA_VS),
    
    .clk_25(clk_25),
    .SW_12(SW_12)
  );

endmodule
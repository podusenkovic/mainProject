module system_top(
  input   CLK,
  input   RST,

  inout [15:0] GPIO,

  // JTAG
  input		TCK,
  input		TMS,
  input		TDI,
  output	TDO,
  
  input [11:0] SW_12,  
  
  output led,

  input RX,
  output TX,

  input SPI_MISO,
  output SPI_CLK,
  output SPI_MOSI,
  output SPI_CS_n,
    
    output   [3:0]   VGA_R,
    output   [3:0]   VGA_G,
    output   [3:0]   VGA_B,
    output           VGA_HS,
    output           VGA_VS
  );
  
  

  

wire reset_n = RST;

wire clk_32;
wire clk_50;
wire clk_25;

clk_wiz_0 pll_in(
  .reset(~reset_n),
  .clk_out1(clk_50),
  .clk_in1(CLK),
  .clk_out_25(clk_25)
  );




/******* generate clock 32768Hz ******/
reg [15:0] cnt_clk;
always @(posedge clk_50 or negedge reset_n)
  if(!reset_n) cnt_clk <= 0;
  else cnt_clk <= (cnt_clk == 762)? 0 : cnt_clk + 1;

reg clk_32_r;
always @(posedge clk_50 or negedge reset_n)
  if(!reset_n) clk_32_r <= 0;
  else clk_32_r <= (cnt_clk == 762)? !clk_32_r : clk_32_r;


`ifdef TEST_FPGA
assign clk_32 = clk_32_r;
`else
BUFG BUFG_CLK32 (
  .I(clk_32_r),
  .O(clk_32)
  );
`endif


    
reg [9:0] cnt;    
always @(posedge clk_32 or negedge reset_n)
  if (!reset_n) cnt <= 0;
  else cnt <= cnt + 1;
  
assign led = cnt[9];  
     
/**************************************/

wire [15:0] gpio_do, gpio_di, gpio_en;

genvar gi;
generate for (gi = 0; gi < 16; gi = gi + 1) begin : gen_gpio
  assign GPIO[gi] = gpio_en[gi]? gpio_do[gi] : 1'bz;
end
endgenerate

wire mem_access;
wire hwrite;
wire burst_length;
wire [1:0] bus_transaction;
wire [2:0] hburst;
wire [31:0] haddr;
wire [31:0] hwdata;
wire [31:0] hrdata;
wire reply_ext;
wire hburst_err_ext;
/*
(* KEEP = "TRUE" *) wire mem_access_r;
(* KEEP = "TRUE" *) wire hwrite_r;
(* KEEP = "TRUE" *) wire burst_length_r;
(* KEEP = "TRUE" *) wire [1:0] bus_transaction_r;
(* KEEP = "TRUE" *) wire [2:0] hburst_r;
(* KEEP = "TRUE" *) wire [31:0] haddr_r;
(* KEEP = "TRUE" *) wire [31:0] hwdata_r;
(* KEEP = "TRUE" *) wire [31:0] hrdata_r;
(* KEEP = "TRUE" *) wire reply_ext_r;
(* KEEP = "TRUE" *) wire hburst_err_ext_r;

assign mem_access_r = mem_access;
assign hwrite_r = hwrite;
assign burst_length_r = burst_length;
assign bus_transaction_r = bus_transaction;
assign hburst_r = hburst;
assign haddr_r = haddr;
assign hwdata_r = hwdata;
assign hrdata_r = hrdata;
assign reply_ext_r = reply_ext;
assign hburst_err_ext_r = hburst_err_ext;*/

system system(
  .clk_cpu     (clk_50),
  .clk_32768   (clk_32),
  .pwr_rst_in_n(reset_n),

  .gpio_do(gpio_do),
  .gpio_en(gpio_en),
  .gpio_di(GPIO),

  .tck(TCK),
  .tms(TMS),
  .tdi(TDI),
  .tdo(TDO),


  .start_mode(2'b00),//STM),

  .mem_access     (mem_access     ),
  .hwrite         (hwrite         ),
  .burst_length   (burst_length   ),
  .bus_transaction(bus_transaction),
  .hburst         (hburst         ), 
  .haddr          (haddr          ),
  .hwdata         (hwdata         ),
  .hrdata_ext     (hrdata         ),
  .reply_ext      (reply_ext      ),
  .hburst_err_ext (hburst_err_ext ),

  .irq_ext         (1'b0)
  );


interconnect interconnect_inst (

  .clk             (clk_50),
  .reset_n         (reset_n),
  .mem_access      (mem_access),
  .hwrite          (hwrite),
  .burst_length    (burst_length),
  .bus_transaction (bus_transaction),
  .hburst          (hburst), 
  .haddr           (haddr),
  .hwdata          (hwdata),
  .hrdata          (hrdata),
  .reply_ext       (reply_ext),
  .hburst_err_ext  (hburst_err_ext), 

  .tx_0            (TX),
  .rx_0            (RX),
  
  
  .SPI_MISO(SPI_MISO),
  .SPI_CLK(SPI_CLK),
  .SPI_MOSI(SPI_MOSI),
  .SPI_CS_n(SPI_CS_n),
    .VGA_R(VGA_R),
    .VGA_G(VGA_G),
    .VGA_B(VGA_B),
    .VGA_HS(VGA_HS),
    .VGA_VS(VGA_VS),
    .clk_25(clk_25),
    .SW_12(SW_12)
);


endmodule

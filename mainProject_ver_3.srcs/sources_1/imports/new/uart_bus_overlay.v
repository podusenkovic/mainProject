`timescale 1ns / 1ps
module uart_bus_overlay#(
	parameter DATA_WIDTH = 8
	)(
	//SysBus KVARK
	input			BusClk,
	input			Rst,
	input	[31:0]	Addr,
	input	[2:0]	Burst,
	input	[1:0]	Trans,
	input			BurstLen,
	input			MemAccess,
	input			Write,
	input	[31:0]	Wdata,

	output			Ready,
	output	[31:0]	RData,

	//UART signals
	input			rx,
	output			tx,

	output hburst_err_ext
);

reg	[31:0]	Addr_r;
reg	[2:0]	Burst_r;
reg	[1:0]	Trans_r;
reg			BurstLen_r;
reg			MemAccess_r;
reg			Write_r;

reg [DATA_WIDTH-1:0] s_axis_tdata_i;
reg s_axis_tvalid_i;
wire s_axis_tready_o;

wire [DATA_WIDTH-1:0] m_axis_tdata_o;
wire m_axis_tvalid_o;
wire m_axis_tready_i;

wire tx_busy_o;
wire rx_busy_o;
wire rx_overrun_error_o;
wire rx_frame_error_o;

reg [15:0] prescale_r = 16'b0;

reg [31:0] RData_r;

assign Ready = 1'b1;
assign hburst_err_ext = 1'b0;
assign m_axis_tready_i = (Trans_r == 2'b11 && MemAccess_r && ~Write_r && Addr_r[3:0] == 4'h0 && m_axis_tvalid_o == 1'b1);
assign RData = RData_r;


always @(posedge BusClk or posedge Rst) begin
	if (Rst) begin
		Addr_r <= 'b0;
		Burst_r <= 'b0;
		Trans_r <= 'b0;
		BurstLen_r <= 'b0;
		MemAccess_r <= 'b0;
		Write_r <= 'b0;
	end
	else begin
		Addr_r <= Addr;
		Burst_r <= Burst;
		Trans_r <= Trans;
		BurstLen_r <= BurstLen;
		MemAccess_r <= MemAccess;
		Write_r <= Write;
	end
end

always @(posedge BusClk or posedge Rst) begin
	if (Rst) begin
		RData_r <= 32'b0;
	end
	else if (Trans == 2'b11 && MemAccess && ~Write) begin
		case (Addr[3:0])
			4'h0: if(m_axis_tvalid_o == 1'b1) RData_r <= {24'b0, m_axis_tdata_o};
			4'h8: RData_r <= {16'b0, prescale_r};
			4'hC: RData_r <= {28'b0, rx_frame_error_o, rx_overrun_error_o, s_axis_tready_o, m_axis_tvalid_o};
			default: RData_r <= 32'b0;
		endcase
	end
end

always @(posedge BusClk or posedge Rst) begin
	if (Rst) begin
		s_axis_tdata_i <= 'b0;
		s_axis_tvalid_i <= 'b0;
		prescale_r <= 'b0;
	end
	else if (Trans_r == 2'b11 && MemAccess_r && Write_r) begin
		case (Addr_r[3:0])
			4'h4: begin
				s_axis_tdata_i <= Wdata[DATA_WIDTH-1:0];
				s_axis_tvalid_i <= 1'b1;
			end
			4'h8: begin
				prescale_r <= Wdata[15:0];
				s_axis_tvalid_i <= 1'b0;
			end
		endcase
	end 
	else s_axis_tvalid_i <= 1'b0;
end

uart #(DATA_WIDTH) inst (
	.clk(BusClk),	//input
	.rst(Rst),		//input
	
	.s_axis_tdata(s_axis_tdata_i),	//input [DATA_WIDTH-1:0]
	.s_axis_tvalid(s_axis_tvalid_i),	//input
	.s_axis_tready(s_axis_tready_o),	//output
	
	.m_axis_tdata(m_axis_tdata_o),	//output [DATA_WIDTH-1:0]
	.m_axis_tvalid(m_axis_tvalid_o),	//output
	.m_axis_tready(m_axis_tready_i),	//input

	.rxd(rx),
	.txd(tx),

	.tx_busy(tx_busy_o),	//out
	.rx_busy(rx_busy_o),	//out
	.rx_overrun_error(rx_overrun_error_o),	//out
	.rx_frame_error(rx_frame_error_o),	//out

	.prescale(prescale_r)	//in [15:0]
);
endmodule

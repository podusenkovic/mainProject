`timescale 1ns / 1ps
module tb_spi_overlay;

reg clk = 1'b0;
reg rst = 1'b0;
reg [31:0] addr = 32'b0;
reg SPI_MISO = 1'bz;
reg memAccess = 1'b0;
reg [31:0] wdata = 32'b0;
reg write = 1'b0;

reg [4:0] cnt_spi_clk = 5'b0;

wire SPI_CLK;
wire SPI_MOSI;
wire SPI_CS_n;
wire [31:0] rdata;
wire reply;
wire error;

reg [7:0] testValue = 8'hAD;

initial begin
	clk = 1'b0;
	forever #5 clk = !clk;
end

always @(posedge SPI_CLK or posedge rst) begin
	if (rst) begin
		cnt_spi_clk <= 5'b0;
	end
	else begin 
		if (cnt_spi_clk > 5'd15 && cnt_spi_clk != 5'd23 && !SPI_CS_n) begin
			testValue <= testValue << 1;
			cnt_spi_clk <= cnt_spi_clk + 1'b1;
		end
		else if (cnt_spi_clk == 5'd23) begin 
			testValue <= testValue << 1;
			cnt_spi_clk <= 5'd0;
		end
		else cnt_spi_clk <= cnt_spi_clk + 1'b1;
		
	end
end

always @(negedge SPI_CLK or posedge rst) begin
	if (rst) begin
		SPI_MISO <= 1'bz;
	end
	else begin 
		if (cnt_spi_clk > 5'd15 && cnt_spi_clk != 5'd24 && !SPI_CS_n) begin
			SPI_MISO <= testValue[7];
		end
		else if (cnt_spi_clk == 5'd0) begin 
			SPI_MISO <= 5'bz;
		end
	end
end

initial begin
	rst = 1'b1;
	#15;
	rst = 1'b0;
	#10;
	memAccess = 1;
	write = 1;
	wdata = 32'h0000_0b09;
	addr = 32'hfffc_2014;
	#10;
	write = 0;
	#100;
	addr = 32'hfffc_201c;
	while(!(rdata & 32'h0000_0002)) #5;
	$display("Sended! %h", wdata[15:0]);
	#500;
end

spi_overlay spi_overlay_inst(
	.clk(clk),
	.rst(rst),
	.SPI_MISO(SPI_MISO),
	.addr(addr),
	.memAccess(memAccess),
	.wdata(wdata),
	.write(write),

	.SPI_CLK(SPI_CLK),
	.SPI_MOSI(SPI_MOSI),
	.SPI_CS_n(SPI_CS_n),
	.rdata(rdata),
	.reply(reply),
	.error(error)
);
endmodule

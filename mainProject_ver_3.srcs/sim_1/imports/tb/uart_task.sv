module uart_task(
  input  RX,
  output reg TX
  );

parameter UART_BAUDRATE = 115200;

initial forever
  wait (RX == 0)
    uart_rx (UART_BAUDRATE,0,0);


/* ****************** command script *************** */
initial
  begin
  TX = 1; #10us
  uart_tx('h66,UART_BAUDRATE,0,0);
  uart_tx('h01,UART_BAUDRATE,0,0);
  uart_tx('h02,UART_BAUDRATE,0,0);
  uart_tx('h03,UART_BAUDRATE,0,0);
  uart_tx('h04,UART_BAUDRATE,0,0);
  uart_tx('h55,UART_BAUDRATE,0,0);
  uart_tx('h46,UART_BAUDRATE,0,0);
  uart_tx('h37,UART_BAUDRATE,0,0);
  uart_tx('hff,UART_BAUDRATE,0,1);
  uart_tx('haa,UART_BAUDRATE,0,1);
  end
/* ************************************************* */


task uart_rx;
  input integer BAUD;
  input CS;
  input EVEN;
  
  begin
    reg [7:0] DAT;
    reg PARITY, STOP;

    #(1s/(2*BAUD));
    if(RX == 0)
      begin
      #(1s/BAUD) DAT[0] = RX;
      #(1s/BAUD) DAT[1] = RX;
      #(1s/BAUD) DAT[2] = RX;
      #(1s/BAUD) DAT[3] = RX;
      #(1s/BAUD) DAT[4] = RX;
      #(1s/BAUD) DAT[5] = RX;
      #(1s/BAUD) DAT[6] = RX;
      #(1s/BAUD) DAT[7] = RX;

      if (CS) #(1s/BAUD) PARITY = RX;
      #(1s/BAUD) STOP   = RX;
      
      if (STOP)
        if (CS)
          if (EVEN)
            if (^DAT == PARITY) $display("Receive data: %h",DAT);
            else $display("PARITY ERROR");
          else
            if (^DAT == !PARITY) $display("Receive data: %h",DAT);
            else $display("PARITY ERROR");
        else $display("Receive data: %h",DAT);
      else $display("STOP ERROR");
      end
    else $display("GLITCH ERROR");
  end
endtask //uart_rx


task uart_tx;
  input [7:0] DAT;
  input integer BAUD;
  input CS;
  input EVEN;

  begin
    $display("Transfer data: %h",DAT);
    TX = 0;
    #(1s/BAUD) TX = DAT[0];
    #(1s/BAUD) TX = DAT[1];
    #(1s/BAUD) TX = DAT[2];
    #(1s/BAUD) TX = DAT[3];
    #(1s/BAUD) TX = DAT[4];
    #(1s/BAUD) TX = DAT[5];
    #(1s/BAUD) TX = DAT[6];
    #(1s/BAUD) TX = DAT[7];
    
    if (CS)
      if (EVEN) #(1s/BAUD) TX = ^DAT;
      else #(1s/BAUD) TX = ~^DAT;
    
    #(1s/BAUD) TX = 1;
    #(1s/BAUD);
  end
endtask //uart_tx

endmodule
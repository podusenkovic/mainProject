`timescale 1ns / 1ps

module tb ();


//***************** clocks ********************//
parameter PERIOD = 10;

reg RST, CLK;

initial
    begin
    RST = 0;
    #(PERIOD*100) RST = 1;
    end

initial
    begin
    CLK = 0;
    forever #(PERIOD/2) CLK = ~CLK;
    end

//*********************************************//


  wire [15:0] GPIO;

  reg   TCK;
  reg   TMS;
  reg   TDI;
  wire  TDO;

  reg SPI_MISO;
  wire SPI_CLK;
  wire SPI_MOSI;
  wire SPI_CS_n;

  reg [4:0] cnt_spi_clk = 5'b0;
  reg [7:0] testValue = 8'hAD;

initial
  begin
    TCK = 0;
    TMS = 0;
    TDI = 0;
    SPI_MISO = 1'bz;
  end

always @(posedge SPI_CLK or negedge RST) begin
  if (~RST) begin
    cnt_spi_clk <= 5'b0;
  end
  else begin 
    if (cnt_spi_clk > 5'd15 && cnt_spi_clk != 5'd23 && !SPI_CS_n) begin
      testValue <= testValue << 1;
      cnt_spi_clk <= cnt_spi_clk + 1'b1;
    end
    else if (cnt_spi_clk == 5'd23) begin 
      testValue <= 8'h6F;
      cnt_spi_clk <= 5'd0;
    end
    else cnt_spi_clk <= cnt_spi_clk + 1'b1;
    
  end
end

always @(negedge SPI_CLK or negedge RST) begin
  if (~RST) begin
    SPI_MISO <= 1'bz;
  end
  else begin 
    if (cnt_spi_clk > 5'd15 && cnt_spi_clk != 5'd24 && !SPI_CS_n) begin
      SPI_MISO <= testValue[7];
    end
    else if (cnt_spi_clk == 5'd0) begin 
      SPI_MISO <= 5'bz;
    end
  end
end

  wire led;

  wire RX;
  wire TX;


system_top DUT(
  .CLK  (CLK),
  .RST  (RST),

  .GPIO (GPIO),

  .TCK  (TCK),
  .TMS  (TMS),
  .TDI  (TDI),
  .TDO  (TDO),

  .led  (led),

  .RX   (RX),
  .TX   (TX),

  .SPI_MISO(SPI_MISO),
  .SPI_CLK(SPI_CLK),
  .SPI_MOSI(SPI_MOSI),
  .SPI_CS_n(SPI_CS_n)
  );
  

uart_task UART(
  .RX (TX),
  .TX (RX)
  );


endmodule
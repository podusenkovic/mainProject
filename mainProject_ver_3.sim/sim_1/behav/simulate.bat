@echo off
set xv_path=C:\\Xilinx\\Vivado\\2016.4\\bin
call %xv_path%/xsim SPI_Master_With_Single_CS_TB_behav -key {Behavioral:sim_1:Functional:SPI_Master_With_Single_CS_TB} -tclbatch SPI_Master_With_Single_CS_TB.tcl -log simulate.log
if "%errorlevel%"=="0" goto SUCCESS
if "%errorlevel%"=="1" goto END
:END
exit 1
:SUCCESS
exit 0
